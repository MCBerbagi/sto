<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// API Login
Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    // API Document Tenant
    Route::resource('/document', App\Http\Controllers\API\DocumentController::class)->except(['create', 'edit']);

    // API Profile
    Route::get('/profile', [App\Http\Controllers\API\AuthController::class, 'profile']);

    // API Logout
    Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);
});
