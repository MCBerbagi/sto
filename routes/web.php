<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Models\Document;
use App\Http\Controllers\DocumentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/pdf', function () {
    return view('pages.contract.pdf', [
        'document' => Document::first()
    ]);
});

Auth::routes();

Route::group(['middleware' => ['auth']], function (){
    // User
    Route::resource('user', UserController::class);

    // Input Tiket
    // Route::get('/ticket', [App\Http\Controllers\DocumentController::class, 'index'])->name('document.index');

    // Document
    Route::resource('document', DocumentController::class);
    Route::get('/document', [App\Http\Controllers\DocumentController::class, 'index'])->name('document.index');
    Route::any('/create-document', [App\Http\Controllers\DocumentController::class, 'create'])->name('document.create');
    Route::any('/update-document/{id}', [App\Http\Controllers\DocumentController::class, 'update'])->name('document.update');
    Route::any('/report-document/{id}', [App\Http\Controllers\DocumentController::class, 'report'])->name('document.report');
    Route::get('/document/{document}', [App\Http\Controllers\DocumentController::class, 'show'])->name('document.show');
    Route::post('/document/{document}/approve', [App\Http\Controllers\DocumentController::class, 'approve'])->name('document.approve');
    Route::post('/document/{document}/reject', [App\Http\Controllers\DocumentController::class, 'reject'])->name('document.reject');
    Route::get('/document-report/{document}', [App\Http\Controllers\DocumentController::class, 'showReport'])->name('document.show-report');
    Route::get('/document/{document}/pdf', [App\Http\Controllers\DocumentController::class, 'exportPdf'])->name('document.pdf');

    // Document Images
    Route::post('/document/remove-image', [App\Http\Controllers\DocumentController::class, 'removeImage'])->name('document.remove-image');

    // History
    Route::get('/history', [App\Http\Controllers\HistoryController::class, 'index'])->name('history.index');
    Route::get('/history/{document}', [App\Http\Controllers\HistoryController::class, 'show'])->name('history.show');
    Route::get('/history-report/{document}', [App\Http\Controllers\HistoryController::class, 'showReport'])->name('history.show-report');

    // Contract
    Route::get('/contract', [App\Http\Controllers\ContractController::class, 'index'])->name('contract.index');
    Route::get('/contract/{document}', [App\Http\Controllers\ContractController::class, 'show'])->name('contract.show');
    Route::get('/contract/{document}/pdf', [App\Http\Controllers\ContractController::class, 'exportPdf'])->name('contract.pdf');

    // Profile
    Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile.index');
    Route::post('/profile', [App\Http\Controllers\ProfileController::class, 'update'])->name('profile.update');

    // Table
    Route::get('/table/data-report', [App\Http\Controllers\HomeController::class, 'data'])->name('data-report');
});

// Dashboard
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
