<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentDetail extends Model
{
    use HasFactory;

    protected $casts = [
        'images_sebelum' => 'array',
        'images_proses' => 'array',
        'images_sesudah' => 'array'
    ];
}
