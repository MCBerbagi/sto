<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\DocumentApproval;
use App\Models\DocumentDetail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use DateTime;
use PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use PhpParser\Comment\Doc;
use Intervention\Image\Facades\Image;
use Illuminate\Http\File;

class DocumentController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()){
            $query = Document::query();

            if (Auth::user()->hasRole('Manager')) {
                $query->where('status', '=', 'Finish');
            } else if (Auth::user()->hasRole('Admin')) {
                $query->where('status', '!=', 'Approve');
            } else if  (Auth::user()->hasRole('Mitra')) {
                $query->where('status', '!=', 'Approve')
                ->where('mitra_id', Auth::user()->id);
            }

            return DataTables::of($query)
                ->addColumn('mitra', function ($model) {
                    $mitra = User::where('id', $model->mitra_id)->first();
                    return $mitra->name;
                })
                ->editColumn('created_at', function ($model) {
                    return date('H:i d/m/Y', strtotime($model->created_at));
                })
                ->editColumn('status', function ($model) {
                    $response = "";
                    if ($model->status == 'Reject') {
                        $response = "<span class='badge badge-danger'>Rejected</span>";
                    }elseif ($model->status == 'Survey') {
                        $response = "<span class='badge badge-warning'>Survey</span>";
                    }elseif ($model->status == 'Progress') {
                        $response = "<span class='badge badge-secondary'>Progress</span>";
                    }elseif ($model->status == 'Finish') {
                        $response = "<span class='badge badge-primary'>Finished</span>";
                    }elseif ($model->status == 'Approve') {
                        $response = "<span class='badge badge-success'>Manager Approved</span>";
                    }

                    return $response;
                })
                ->editColumn('action', function ($model) {
                    $response = "<div class='text-center'>";

                    $btn_approve = 0;
                    if (Auth::user()->hasRole('Manager') && $model->status == 'Finish') {
                        $btn_approve = 1;
                    }

                    if ($btn_approve == 1) {
                        $response .= "
                            <button onclick=\"approvalSubmitAction('".route('document.approve', $model['id'])."')\" class='btn btn-sm btn-light-success btn-circle btn-icon mr-2' title='Approve'><i class='far fa-thumbs-up icon-nm'></i></button>
                            <button onclick=\"approvalSubmitAction('".route('document.reject', $model['id'])."')\" class='btn btn-sm btn-light-danger btn-circle btn-icon mr-2' title='Reject'><i class='far fa-thumbs-down icon-nm'></i></button>
                        ";
                    }

                    $response .= "<a href='javascript:void(0);' data-href='".route('document.show', $model['id'])."' class='btn btn-sm btn-light-primary btn-circle btn-icon mr-2 loadModal' title='Detail Tiket'><i class='fas fa-list icon-nm'></i></a>
                    <a href='javascript:void(0);' data-href='".route('document.show-report', $model['id'])."' class='btn btn-sm btn-light-primary btn-circle btn-icon mr-2 loadModal' title='Detail Report'><i class='fas fa-book icon-nm'></i></a>

                    ";

                    if (Auth::user()->hasRole('Admin') && $model->status != 'Approve'){
                        $response .= "<a href='javascript:void(0);' data-href='".route('document.update', $model['id'])."' class='btn btn-sm btn-light-primary btn-circle btn-icon mr-2 loadModal' title='Edit'><i class='fas fa-edit icon-nm'></i></a>
                    <a href='".route('document.destroy', $model['id'])."' class='btn btn-sm btn-light-danger btn-circle btn-icon mr-2 btn-delete' title='Delete'><i class='fas fa-trash-alt icon-nm'></i></a>
                        </div>";
                    }
                    else if (Auth::user()->hasRole('Admin') && $model->status != 'Survey'){
                        $response .= "
                   </div>";
                    }

                    else if (Auth::user()->hasRole('Mitra') && $model->status != 'Approve' && $model->status != 'Finish') {
                        $response .= "<a href='javascript:void(0);' data-href='".route('document.report', $model['id'])."' class='btn btn-sm btn-light-primary btn-circle btn-icon mr-2 loadModal' title='Add Report'><i class='fas fa-plus icon-nm'></i></a>
                        </div>";
                    }else if ($model->status == 'Approve') {
                        $response .= "<a href='".route('document.pdf', $model['id'])."' class='btn btn-sm btn-light-danger btn-circle btn-icon mr-2' title='Export PDF'><i class='fas fa-file-pdf icon-nm'></i></a>
                        </div>";
                    }

                    return $response;
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        }

        return view('pages.document.index');
    }

    public function show(Document $document)
    {
        $mitra = User::where('id', $document->mitra_id)->first();
        return view('pages.document.show', [
            'data' => $document,
            'mitra' => $mitra
        ]);
    }

    public function showReport(Document $document)
    {
        $document_detail = DocumentDetail::where('document_id', $document->id)->first();
        return view('pages.history.show-report', [
            'document' => $document,
            'document_detail' => $document_detail
        ]);
    }

    public function create(Request $request) {
        if ($request->isMethod('post')) {
            try {
                $validator = $this->validation($request);
                if (!$validator->fails()) {
                    $store = $this->storeDocument($request);
                    if (!$store->error) {
                        $error = $store->error;
                        $message = $store->message;
                        return response()->json(compact('error', 'message'), 200);
                    } else {
                        $error = $store->error;
                        $message = $store->message;
                        return response()->json(compact('error', 'message'), 400);

                    }
                } else {
                    $error = true;
                    $message = $validator->messages();
                    return response()->json(compact('error', 'message'), 400);
                }
            } catch (\Throwable $th) {

                Log::debug("Error create product");
                Log::debug($th);
            }
        }
        $mitra = User::whereHas("roles", function($q){ $q->where("name", "Mitra"); })->get();
        return view('pages.document.create', compact('mitra'));
    }

    public function update(Request $request, $id) {
        if ($request->isMethod('post')) {
            $validator = $this->validation($request);
            if (!$validator->fails()) {
                $update = $this->updateDocument($id, $request);
                if (!$update->error) {
                    $error = $update->error;
                    $message = $update->message;
                    return response()->json(compact('error', 'message'), 200);
                } else {
                    $error = $update->error;
                    $message = $update->message;
                    return response()->json(compact('error', 'message'), 400);
                }
            } else {
                $error = true;
                $message = $validator->messages();
                return response()->json(compact('error', 'message'), 400);
            }
        }

        $document = Document::find($id);
        $mitra = User::whereHas("roles", function($q){ $q->where("name", "Mitra"); })->get();
        $nomor_tiket = explode('IN', $document->nomor_tiket);
        return view('pages.document.edit', compact('document', 'mitra','nomor_tiket'));
    }

    public function report(Request $request, $id) {
        if ($request->isMethod('post')) {
            // Log::debug($request);
            $validator = $this->validationReport($request);
            if (!$validator->fails()) {
                $update = $this->reporting($id, $request);
                if (!$update->error) {
                    $error = $update->error;
                    $message = $update->message;
                    return response()->json(compact('error', 'message'), 200);
                } else {
                    $error = $update->error;
                    $message = $update->message;
                    return response()->json(compact('error', 'message'), 400);
                }
            } else {
                $error = true;
                $message = $validator->messages();
                return response()->json(compact('error', 'message'), 400);
            }
        }

        $document = Document::find($id);
        $document_detail = DocumentDetail::where('document_id', $id)->first();
        // $images = $document_detail->images_sebelum;
        // Log::debug(gettype(($images[0])));
        $status = ['Survey', 'Progress', 'Finish'];
        return view('pages.document.report', compact('document', 'document_detail', 'status'));
    }

    public function validationReport($data) {
        $param = [
            'sebelum1' => 'sometimes',
            'sebelum2' => 'sometimes',
            'sebelum3' => 'sometimes',
            'proses1' => 'sometimes',
            'proses2' => 'sometimes',
            'proses3' => 'sometimes',
            'sesudah1' => 'sometimes',
            'sesudah2' => 'sometimes',
            'sesudah3' => 'sometimes',
            'img_sebelum1' => 'exclude_if:sebelum1,null|mimes:jpeg,png,jpg',
            'img_sebelum2' => 'exclude_if:sebelum2,null|mimes:jpeg,png,jpg',
            'img_sebelum3' => 'exclude_if:sebelum3,null|mimes:jpeg,png,jpg',
            'img_proses1' => 'exclude_if:proses1,null|mimes:jpeg,png,jpg',
            'img_proses2' => 'exclude_if:proses2,null|mimes:jpeg,png,jpg',
            'img_proses3' => 'exclude_if:proses3,null|mimes:jpeg,png,jpg',
            'img_sesudah1' => 'exclude_if:sesudah1,null|mimes:jpeg,png,jpg',
            'img_sesudah2' => 'exclude_if:sesudah2,null|mimes:jpeg,png,jpg',
            'img_sesudah3' => 'exclude_if:sesudah3,null|mimes:jpeg,png,jpg',
        ];

        $valMessage = [
            'img_sebelum1.mimes' => 'Tipe file jpeg, png',
            'sebelum2.mimes' => 'Tipe file jpeg, png',
            'sebelum3.mimes' => 'Tipe file jpeg, png',
            'proses1.mimes' => 'Tipe file jpeg, png',
            'proses2.mimes' => 'Tipe file jpeg, png',
            'proses3.mimes' => 'Tipe file jpeg, png',
            'sesudah1.mimes' => 'Tipe file jpeg, png',
            'sesudah2.mimes' => 'Tipe file jpeg, png',
            'sesudah3.mimes' => 'Tipe file jpeg, png',
        ];

        $validator = Validator::make($data->all(), $param, $valMessage);
        return $validator;
    }

    public function validation($data) {
        $param = [
            'nomor_tiket' => 'required|numeric',
            'sto' => 'required',
            'detail_gamas' => 'required',
            'node_terganggu' => 'required',
            'mitra_id' => 'required',
            'nama' => 'required',
        ];

        $valMessage = [
            'nomor_tiket.required' => 'Nomor tiket wajib diisi',
            'nomor_tiket.numeric' => 'Nomor tiket wajib angka',
            'sto.required' => 'STO wajib diisi',
            'detail_gamas.required' => 'Detail gamas wajib diisi',
            'node_terganggu.required' => 'Node terganggu wajib diisi',
            'mitra_id.required' => 'Mitra wajib diisi',
            'nama.required' => 'Nama wajib diisi',
        ];

        $validator = Validator::make($data->all(), $param, $valMessage);
        return $validator;
    }

    public function storeDocument($request) {
        $output = new \stdClass;
        DB::beginTransaction();
        try {
            $document = new Document();
            $nomor_tiket = 'IN';
            for ($i = 0; $i < (8 - strlen($request->nomor_tiket)); $i++) {
                $nomor_tiket .= '0';
            }
            $nomor_tiket .= $request->nomor_tiket;
            $document->nomor_tiket = $nomor_tiket;
            $document->sto = $request->sto;
            $document->detail_gamas = $request->detail_gamas;
            $document->node_terganggu = $request->node_terganggu;
            $document->mitra_id = $request->mitra_id;
            $document->nama = $request->nama;
            $document->status = 'Survey';
            $document->created_by = Auth::guard('sanctum')->user()->id;
            $document->save();

            $documentDetail = new DocumentDetail();
            $documentDetail->document_id = $document->id;
            $documentDetail->images_sebelum = [];
            $documentDetail->images_proses = [];
            $documentDetail->images_sesudah = [];
            $documentDetail->save();

            $output->error = false;
            $output->message = 'success';
            DB::commit();
            // all good
        } catch (\Exception $e) {
            Log::debug("Error storeProductStock()");
            Log::debug($e);
            $output->error = true;
            $output->message = 'failed';
            DB::rollback();
            // something went wrong
        }
        return $output;
    }

    public function updateDocument($id, $request)
    {
        $output = new \stdClass;
        DB::beginTransaction();
        try {
            $document = Document::find($id);
            $nomor_tiket = 'IN';
            for ($i = 0; $i < (8 - strlen($request->nomor_tiket)); $i++) {
                $nomor_tiket .= '0';
            }
            $nomor_tiket .= $request->nomor_tiket;
            $document->nomor_tiket = $nomor_tiket;
            $document->sto = $request->sto;
            $document->detail_gamas = $request->detail_gamas;
            $document->node_terganggu = $request->node_terganggu;
            $document->mitra_id = $request->mitra_id;
            $document->nama = $request->nama;
            $document->save();

            $output->error = false;
            $output->message = 'success';
            DB::commit();
            // all good
        } catch (\Exception $e) {
            $output->error = true;
            $output->message = 'failed';
            DB::rollback();
        }
        return $output;
    }

    public function destroy(Document $document)
    {
        try {
            $document->delete();
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage());
        }

        return back()->withMessage('Delete user successfully');
    }


    public function reporting($id, $request)
    {
        $output = new \stdClass;
        DB::beginTransaction();
        try {
            $document_detail = DocumentDetail::where('document_id',$id)->first();

            if ($request->start_date != null) {
                $start_date = DateTime::createFromFormat('d/m/Y H:i', $request->start_date)->format('Y-m-d H:i');
                $document_detail->start_date = $start_date;
            }
            if ($request->end_date != null) {
                $end_date = DateTime::createFromFormat('d/m/Y H:i', $request->end_date)->format('Y-m-d H:i');
                $document_detail->end_date = $end_date;
            }
            $document_detail->keterangan = $request->keterangan;
            // $document_detail->images_sebelum = ['foo', 'bar'];

            $document_detail->save();

            $this->saveImages($document_detail, $request);

            $document = Document::find($id);
            $document->status = $request->status ?? 'Survey';
            if ($request->status == 'Finish') {
                $date_now = Carbon::now()->format('Y-m-d H:i');
                $document_detail->end_date = $date_now;
                $document_detail->save();
            }
            $document->save();

            $output->error = false;
            $output->message = 'success';
            DB::commit();
            // all good
        } catch (\Exception $e) {
            $output->error = true;
            $output->message = 'failed';
            DB::rollback();
            Log::debug($e);
        }
        return $output;
    }

    public function saveImages($document_detail, $request) {
        $sebelum = ['sebelum1', 'sebelum2', 'sebelum3'];
        $proses = ['proses1', 'proses2', 'proses3'];
        $sesudah = ['sesudah1', 'sesudah2', 'sesudah3'];

        $images = [$sebelum, $proses, $sesudah];
        $loc = ['sebelum', 'proses', 'sesudah'];

        $path_img_sebelum = [];
        $path_img_proses = [];
        $path_img_sesudah = [];

        foreach ($document_detail->images_sebelum as $key => $value) {
            array_push($path_img_sebelum,  $value);
        }
        foreach ($document_detail->images_proses as $key => $value) {
            array_push($path_img_proses,  $value);
        }
        foreach ($document_detail->images_sesudah as $key => $value) {
            array_push($path_img_sesudah,  $value);
        }

        $path_img = [$path_img_sebelum, $path_img_proses, $path_img_sesudah];

        foreach ($images as $key => $image) {
            foreach ($image as $img_index => $item) {
                if (isset($request->$item)) {
                    if ($request->hasFile($item) && $request->file($item)->isValid()) {
                        // dd($request->file($item)->hashName());
                        $destinationPath = storage_path("app/public/images/$loc[$key]");

                        $img = $request->file($item);
                        $img = Image::make($img->path());

                        // dd($img);
                        $path = $img->resize(600, 500)->save($destinationPath.'/'. $request->file($item)->hashName());
//                        if (!file_exists($path)) {
//                            mkdir($path, 0755);
//                        }
                        // function ($constraint) {
                        //     $constraint->aspectRatio();
                        // }


                        // $path = $img->move($destinationPath, ($request->$item)->hashName());

                        // $path = Storage::putFile(
                        //     "public/images/$loc[$key]", $request->$item
                        // );

                        // dd($path->basename);
                        $ext = explode('/', $path->basename);
                        if ($img_index < count($path_img[$key])) {
                            Storage::delete("public/images/$loc[$key]/" . $path_img[$key][$img_index]);
                            unset($path_img[$key][$img_index]);
                        }
                        array_push($path_img[$key], end($ext));
                    }
                }
            }
        }

        $document_detail->images_sebelum = array_values($path_img[0]);
        $document_detail->images_proses = array_values($path_img[1]);
        $document_detail->images_sesudah = array_values($path_img[2]);
        $document_detail->save();
    }

    public function removeImage(Request $request)
    {
        $document_detail = DocumentDetail::find($request->id);
        foreach ($document_detail->images_sebelum as $key => $image) {
            if ($image == $request->path) {
                $temp = $document_detail->images_sebelum;
                unset($temp[$key]);
                $document_detail->images_sebelum = $temp;
                Storage::delete('public/images/sebelum/' . $request->path);
            }
        }
        foreach ($document_detail->images_proses as $key => $image) {
            if ($image == $request->path) {
                $temp = $document_detail->images_proses;
                unset($temp[$key]);
                $document_detail->images_proses = $temp;
                Storage::delete('public/images/proses/' . $request->path);
            }
        }
        foreach ($document_detail->images_sesudah as $key => $image) {
            if ($image == $request->path) {
                $temp = $document_detail->images_sesudah;
                unset($temp[$key]);
                $document_detail->images_sesudah = $temp;
                Storage::delete('public/images/sesudah/' . $request->path);
            }
        }

        $document_detail->save();
    }

    public function approve(Document $document)
    {
        try {
            DB::beginTransaction();
            $documentApproval = DocumentApproval::create([
                'document_id' => $document->id,
                'user_id' => Auth::guard('sanctum')->user()->id,
                'position' => Auth::guard('sanctum')->user()->getRoleNames()[0],
                'date' => date('Y-m-d H:i:s'),
                'message' => 'Document Approved by '.Auth::guard('sanctum')->user()->getRoleNames()[0],
                'status' => "Approve",
            ]);

            $document->update([
                'status' => "Approve"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors([$e->getMessage()]);
        }

        DB::commit();
        return redirect()->route('document.index')->withMessage('Document Approved');
    }

    public function reject(Document $document)
    {
        try {
            DB::beginTransaction();
            $documentApproval = DocumentApproval::create([
                'document_id' => $document->id,
                'user_id' => Auth::guard('sanctum')->user()->id,
                'position' => Auth::guard('sanctum')->user()->getRoleNames()[0],
                'date' => date('Y-m-d H:i:s'),
                'message' => 'Document Rejected by '.Auth::guard('sanctum')->user()->getRoleNames()[0],
                'status' => "Reject",
            ]);

            $document->update([
                'status' => "Reject"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors([$e->getMessage()]);
        }

        DB::commit();
        return redirect()->route('document.index')->withMessage('Document Rejected');
    }

    public function exportPdf(Document $document)
    {
        $mitra = User::where('id', $document->mitra_id)->first();
        $document_detail = DocumentDetail::where('document_id', $document->id)->first();

        $path_img_sebelum = [];
        $path_img_proses = [];
        $path_img_sesudah = [];

        foreach ($document_detail->images_sebelum as $key => $value) {
            $img_path = public_path("/storage/images/sebelum/". $value);
            $extension = pathinfo($img_path, PATHINFO_EXTENSION);
            $image_base64 = base64_encode(file_get_contents(public_path("/storage/images/sebelum/". $value)));
            $path_img = 'data:image/' . $extension . ';base64,' . $image_base64;
            array_push($path_img_sebelum,  $path_img);
        }
        foreach ($document_detail->images_proses as $key => $value) {
            $img_path = public_path("/storage/images/proses/". $value);
            $extension = pathinfo($img_path, PATHINFO_EXTENSION);
            $image_base64 = base64_encode(file_get_contents(public_path("/storage/images/proses/". $value)));
            $path_img = 'data:image/' . $extension . ';base64,' . $image_base64;
            array_push($path_img_proses,  $path_img);
        }
        foreach ($document_detail->images_sesudah as $key => $value) {
            $img_path = public_path("/storage/images/sesudah/". $value);
            $extension = pathinfo($img_path, PATHINFO_EXTENSION);
            $image_base64 = base64_encode(file_get_contents(public_path("/storage/images/sesudah/". $value)));
            $path_img = 'data:image/' . $extension . ';base64,' . $image_base64;
            array_push($path_img_sesudah,  $path_img);
        }

        $pdf = PDF::loadView('pages.export.pdf', compact('document', 'mitra', 'document_detail', 'path_img_sebelum', 'path_img_proses', 'path_img_sesudah'));
        return $pdf->download($document->nomor_tiket.'_'.date('YmdHis').'.pdf');
    }
}
