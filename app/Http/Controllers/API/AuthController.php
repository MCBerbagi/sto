<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $errorMessage = "";
        try {
            $request->validate([
                'email' => 'required|email',
                'password' => 'required',
            ]);

            $user = User::where('email', $request->email)->firstOrFail();
            if ($user){
                if (Hash::check($request->password, $user->password)) {
                    if ($user->hasRole('Teknisi')) {
                        $credentials = $request->only('email', 'password');
                        if(Auth::attempt($credentials)){
                            $token = $user->createToken('auth_token')->plainTextToken;

                            return response()->json([
                                'message' => 'Login Success',
                                'data' => [
                                    'token' => $token,
                                    'user' => $user,
                                ],
                            ]);
                        }else{
                            $errorMessage = "Login failed";
                        }
                    } else {
                        $errorMessage = "This user role is not a Teknisi";
                    }
                }else{
                    $errorMessage = "Password is incorrect";
                }
            }else{
                $errorMessage = 'Email not registered';
            }
        } catch (\Exception $e) {
            $errorMessage = $e->getMessage();
        }

        return response()
                ->json([
                    'message'   => $errorMessage,
                    'data'      => [],
                ], 401);
    }

    function profile()
    {
        $user = Auth::user();
        return response()->json([
            'message' => 'Profile Success',
            'data' => $user,
        ]);
    }

    function logout()
    {
        try {
            Auth::guard('sanctum')->user()->tokens()->revoke();
            return response()->json([
                'message' => 'Logout Success',
                'data' => [],
            ]);
        } catch (\Exception $e) {
            return response()
                ->json([
                    'message'   => $e->getMessage(),
                    'data'      => [],
                ], 401);
        }
    }
}
