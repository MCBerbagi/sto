<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function queryDocument()
    {
        $query = Document::query();

        // if (Auth::user()->hasRole('Manager')) {
        //     $query->where('status', '=', 'Finish');
        // }

        return $query;
    }

    public function index()
    {
        $period = now()->subMonths(4)->monthsUntil(now());

        $header = [];
        $chart = [];
        foreach ($period as $date) {
            $header[] = $date->monthName . " " . $date->year;
            $chart[] = $this->queryDocument()->where('created_at', 'LIKE', $date->year . '-' . sprintf("%02d", $date->month) . '%')->count('id');
        }

        $doc_count = [
            'doc_on_progress'   => $this->queryDocument()->where('status', '=', 'Progress')->count('id'),
            'doc_finished'      => $this->queryDocument()->where('status', 'Finish')->count('id'),
            'doc_approved'      => $this->queryDocument()->where('status', 'Approve')->count('id'),
        ];

        $datas = [
            'doc_total' => $this->queryDocument()->count('id'),
            'doc_count' => $doc_count,
            'chart' => [
                'header' => $header,
                'value' => $chart
            ]
        ];

        return view('home', $datas);
    }

    public function data()
    {
        $query = Document::query();

        if (Auth::user()->hasRole('Mitra')) {
            $query->where('status', '=', 'Progress')->orWhere('status', '=', 'Survey')
                ->where('mitra_id', Auth::user()->id);
        }

        return DataTables::of($query)
            ->addColumn('mitra', function ($model) {
                $mitra = User::where('id', $model->mitra_id)->first();
                return $mitra->name;
            })
            ->editColumn('created_at', function ($model) {
                return date('H:i d/m/Y', strtotime($model->created_at));
            })
            ->editColumn('status', function ($model) {
                $response = "";
                if ($model->status == 'Reject') {
                    $response = "<span class='badge badge-danger'>Rejected</span>";
                } elseif ($model->status == 'Survey') {
                    $response = "<span class='badge badge-warning'>Survey</span>";
                } elseif ($model->status == 'Progress') {
                    $response = "<span class='badge badge-secondary'>Progress</span>";
                } elseif ($model->status == 'Finish') {
                    $response = "<span class='badge badge-primary'>Finished</span>";
                } elseif ($model->status == 'Approve') {
                    $response = "<span class='badge badge-success'>Manager Approved</span>";
                }

                return $response;
            })
            ->editColumn('action', function ($model) {
                $response = "<div class='text-center'>
                                <a href='javascript:void(0);' data-href='" . route('history.show', $model['id']) . "' class='btn btn-sm btn-light-primary btn-circle btn-icon mr-2 loadModal' title='Detail Tiket'><i class='fas fa-list icon-nm'></i></a>
                                <a href='javascript:void(0);' data-href='" . route('history.show-report', $model['id']) . "' class='btn btn-sm btn-light-primary btn-circle btn-icon mr-2 loadModal' title='Detail Report'><i class='fas fa-book icon-nm'></i></a>
                            ";
                if ($model->status == 'Approve') {
                    $response .= "<a href='" . route('document.pdf', $model['id']) . "' class='btn btn-sm btn-light-danger btn-circle btn-icon mr-2' title='Export PDF'><i class='fas fa-file-pdf icon-nm'></i></a>";
                }
                $response .= "</div>";

                return $response;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }
}
