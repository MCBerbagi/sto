<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()){
            $query = User::query()
                ->where('id', '!=', Auth::id());

            return DataTables::of($query)
                ->editColumn('action', function ($model) {
                    $response = "
                    <div class='text-center'>
                        <a href='".route('user.edit', $model['id'])."' class='btn btn-sm btn-light-success btn-circle btn-icon mr-2' title='Edit'><i class='fas fa-pen icon-nm'></i></a>
                        <a href='".route('user.destroy', $model['id'])."' class='btn btn-sm btn-light-danger btn-circle btn-icon mr-2 btn-delete' title='Delete'><i class='fas fa-trash-alt icon-nm'></i></a>
                    </div>";

                    return $response;
                })
                ->make(true);
        }

        return view('pages.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.user.create', [
            'roles' => Role::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required',
            'email'     => 'required|email',
            'password'  => 'required|confirmed',
            'role'      => 'required'
        ]);

        DB::beginTransaction();
        try {
            $user = User::create([
                'name'      => $request->name,
                'email'     => $request->email,
                'password'  => Hash::make($request->password)
            ]);

            $user->assignRole([$request->role]);

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors($e->getMessage())->withInput();
        }

        return redirect()->route('user.index')->withMessage('Add user successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function edit(User $user)
    {
        return view('pages.user.edit', [
            'data' => $user,
            'roles' => Role::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name'  => 'required',
            'email'  => 'required',
        ]);

        DB::beginTransaction();

        try {
            $user->update([
                'name'      => $request->name,
                'email'     => $request->email,
            ]);

            $user->syncRoles([$request->role]);
        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors($e->getMessage())->withInput();
        }

        if (isset($request->password) && !empty($request->password)){
            $request->validate([
                'password'  => 'required|min:6|confirmed'
            ]);

            $user->update([
                'password'  => Hash::make($request->password)
            ]);
        }

        DB::commit();
        return redirect()->route('user.index')->withMessage('Update user successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User $user)
    {
        try {
            $user->delete();
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage());
        }

        return back()->withMessage('Delete user successfully');
    }

    public function updateProfile(Request $request)
    {
        try {
            $this->userService->updatePersonalData($request->except('_token'), Auth::user());
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return back()->withMessage('Update personal information successfully');
    }

    public function updatePersonalPassword(Request $request)
    {
        try {
            $this->userService->changePersonalPasswordUser($request->except('_token'), Auth::user());
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return redirect()->route('profile.show')->withMessage('Update personal information successfully');
    }
}
