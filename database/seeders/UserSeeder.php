<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin123'),
        ]);

        $user->assignRole('Admin');

        $user = User::create([
            'name' => 'Mitra',
            'email' => 'mitra@gmail.com',
            'password' => Hash::make('mitra123'),
        ]);

        $user->assignRole('Mitra');

        $user = User::create([
            'name' => 'Manager',
            'email' => 'manager@gmail.com',
            'password' => Hash::make('manager123'),
        ]);

        $user->assignRole('Manager');

    }
}
