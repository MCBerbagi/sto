<div class="modal fade" id="id_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Detail Tiket
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>Nomor Tiket</th>
                                <td>{{$data->nomor_tiket}}</td>
                            </tr>
                            <tr>
                                <th>STO</th>
                                <td>
                                    {{$data->sto}}
                                </td>
                            </tr>
                            <tr>
                                <th>Detail Gamas</th>
                                <td>
                                    {{$data->detail_gamas}}
                                </td>
                            </tr>
                            <tr>
                                <th>Node Terganggu</th>
                                <td>
                                    {{$data->node_terganggu}}
                                </td>
                            </tr>
                            <tr>
                                <th>Mitra</th>
                                <td>
                                    {{$mitra->name}}
                                </td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td>
                                    {{$data->nama}}
                                </td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                    {{$data->status}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
