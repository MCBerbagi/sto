<div class="modal fade" id="id_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="m-form m-form--fit m-form--label-align-right" id="form_data" action="{{route('document.create')}}" method="post">
            {{csrf_field()}}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Tambah Document
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        &times;
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="nomor_tiket">
                                   Nomor Tiket
                                </label>
                                <input type="number" name="nomor_tiket" class="form-control" id="nomor_tiket" value="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="sto">
                                   STO
                                </label>
                                <input type="text" name="sto" class="form-control" id="sto" value="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="detail_gamas">
                                   Detail Gamas
                                </label>
                                <input type="text" name="detail_gamas" class="form-control" id="detail_gamas" value="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="node_terganggu">
                                   Node Terganggu
                                </label>
                                <input type="text" name="node_terganggu" class="form-control" id="node_terganggu" value="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="mitra_id">
                                    Pilih Mitra
                                </label>
                                <select name="mitra_id" id="mitra_id" class="form-control select2me" required>
                                    <option value="">---pilih mitra---</option>
                                    @foreach ($mitra as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="nama">
                                   Nama
                                </label>
                                <input type="text" name="nama" class="form-control" id="nama" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-success" id="btn_submit">
                        Save changes
                    </button>
                </div>
            </div>
        </form>
    </div>
    <script>
        var counter = 0;
        $('.select2me').select2({
            width: '100%',
        });

        $('.summernote').summernote({
            height: 250
        });

        var options = {
            target: '.message',
            beforeSubmit: function () {
                $("#btn_submit").addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
                $("#btn_submit").prop({disabled: true});
            },
            error: function (data) {
                clearValidation();

                $("#btn_submit").removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
                $("#btn_submit").prop({disabled: false});
                const response = data.responseJSON;
                const errors = response.message;
                const items = Object.keys(errors);
                const firstName = items[0];
                const firstItemDOM = document.getElementById(firstName);

                firstItemDOM.scrollIntoView()

                for (let index = 0; index < items.length; index++) {
                    const element = items[index];
                    const message = errors[element];

                    $('.' + element).addClass('has-error');
                    $('[name="'+element+'"]').addClass('field-error');
                    $('<span class="has-error">'+message+'</span>').insertAfter('[id="'+element+'"]');
                }

                swal.fire("Error", 'Silahkan periksa kembali data anda', "error")
            },
            success: function (data) {
                $("#btn_submit").removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
                if (data.error) {
                    $("#btn_submit").prop({disabled: false});
                    swal.fire("Error", data.message, "error")
                } else {
                    $(".modal").modal("hide");
                    table.ajax.reload(null, false);
                    swal.fire("Good job!", data.message, "success")
                }

            },
            dataType: 'json'
        };

        // pass options to ajaxForm
        $('#form_data').ajaxForm(options);
    </script>
</div>
