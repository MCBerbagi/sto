{{-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">

<div class="modal fade" id="id_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Detail Report ({{$document->nomor_tiket}})
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        &times;
                    </button>
                </div>
                <div class="modal-body">
                    {{-- Sebelum --}}
                    <div class="row">
                        <input type="hidden" name="report_id" value="{{ $document_detail->id }}">
                        <div class="col-md-6">
                            <label for="">Tanggal Awal Pengejaan</label>
                            <input disabled type="text" class="form-control datetimepicker1" name="start_date" value="{{ $document_detail->start_date != null ?  DateTime::createFromFormat('Y-m-d H:i:s', $document_detail->start_date)->format('d/m/Y H:i') : ''}}">
                        </div>
                        <div class="col-md-6">
                            <label for="">Tanggal Akhir Pengejaan</label>
                            <input disabled type="text" class="form-control datetimepicker2" name="end_date" value="{{ $document_detail->end_date != null ? DateTime::createFromFormat('Y-m-d H:i:s', $document_detail->end_date)->format('d/m/Y H:i') : '' }}">
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="" style="font-weight: bold; font-size: 15px; margin-top: 10px">
                                   Sebelum
                                </label>
                            </div>
                        </div>
                        @for ($i = 0; $i < count($document_detail->images_sebelum); $i++)
                            <div class="col-md-12">
                                <div class="form-group">
                                    <img src="{{ asset('storage/images/sebelum/'.$document_detail->images_sebelum[$i]) }}" class="img-fluid">
                                </div>
                            </div>
                        @endfor
                        {{-- Proses --}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="" style="font-weight: bold; font-size: 15px">
                                   Proses
                                </label>
                            </div>
                        </div>
                        @for ($i = 0; $i < count($document_detail->images_proses); $i++)
                            <div class="col-md-12">
                                <div class="form-group">
                                    <img src="{{ asset('storage/images/proses/'.$document_detail->images_proses[$i]) }}" class="img-fluid">
                                </div>
                            </div>
                        @endfor
                        {{-- Sesudah --}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="" style="font-weight: bold; font-size: 15px">
                                   Sesudah
                                </label>
                            </div>
                        </div>
                        @for ($i = 0; $i < count($document_detail->images_sesudah); $i++)
                            <div class="col-md-12">
                                <div class="form-group">
                                    <img src="{{ asset('storage/images/sesudah/'.$document_detail->images_sesudah[$i]) }}" class="img-fluid">
                                </div>
                            </div>
                        @endfor
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="status">
                                    Status
                                </label>
                                <input disabled type="text" name="status" class="form-control" id="status" value="{{ $document->status }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="keterangan">
                                   Keterangan
                                </label>
                                <input disabled type="text" name="keterangan" class="form-control" id="keterangan" value="{{ $document_detail->keterangan }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script>
        $('.select2me').select2({
            width: '100%',
        });

        $('.summernote').summernote({
            height: 250
        });

        $(function() {
           $('.datetimepicker1').datetimepicker({
            format: 'dd/mm/yyyy hh:ii'
           });
        });

        $(function() {
           $('.datetimepicker2').datetimepicker({
            format: 'dd/mm/yyyy hh:ii'
           });
        });

        $('textarea').each(function () {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
        });
    </script>
</div>
