@extends('layouts.master')
@section('title', 'List Document')

@push('css-style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/6.0.1/css/tempus-dominus.min.css" integrity="sha512-07ojEa2oINp0lHi9ia8oFgsH1DTAtGZzgLQL6flOGm6k+4LhOLHT01eOC6srv4mx8vkor94HLcgq3hMw9RFuYQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Lists</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
            <a href="" class="text-muted">Document</a>
        </li>
        <li class="breadcrumb-item">
            <span class="text-muted">List</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::Mixed Widget 10-->
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">List Document</h3>
                </div>
                @if (Auth::user()->hasRole('Admin'))
                <div class="card-title">
                    <a href="javascript:void(0);" data-href="{{ route('document.create') }}" class="btn btn-outline-brand btn-bold loadModal">
                        Tambah Document
                    </a>
                </div>
                @endif
            </div>

            <div class="card-body">
                <table class="table table-separate table-head-custom table-checkable" id="datatable">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Dibuat</th>
                        <th>Nomor Tiket</th>
                        <th>STO</th>
                        <th>Mitra</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

            </div>
        </div>
        <!--end::Mixed Widget 10-->
    </div>
</div>

<form action="" method="POST" id="formApproval">
    @csrf
</form>
@endsection

@push('js-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js" integrity="sha512-YUkaLm+KJ5lQXDBdqBqk7EVhJAdxRnVdT2vtCzwPHSweCzyMgYV/tgGF4/dCyqtCC2eCphz0lRQgatGVdfR0ww==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/6.0.1/js/tempus-dominus.min.js" integrity="sha512-M4xiK8ehSw6gosDfvm4fNtPhErcesJOyZ/st7A8V4svgqg9EhIO8X6YeJvB0vc2lC0czFUQrqgMnjHdVTbZrEg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        var table;
        var load = false;

        // jQuery.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });

        // $.fn.dataTable.ext.errMode = 'none';

        var KTDatatablesDataSourceAjaxServer = function () {
            var initTable1 = function () {
                table = $('#datatable');
                table = table.DataTable({
                    responsive: true,
                    scrollX: true,
                    processing: true,
                    serverSide: true,
                    stateSave: false,
                    ajax: "{{ route('document.index') }}",
                    columns: [
                        {data: null, sortable: false, searchable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: 'created_at', name: 'created_at'},
                        {data: 'nomor_tiket', name: 'nomor_tiket'},
                        {data: 'sto', name: 'sto'},
                        {data: 'mitra', name: 'mitra'},
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ],
                    columnDefs: [
                        { "className": "text-center", "targets": 2 },
                    ]
                });
            };

            return {
                //main function to initiate the module
                init: function () {
                    initTable1();
                },
            };
        }();
        // $(document).ready(function () {

        // });
        $(document).ready(function() {
            KTDatatablesDataSourceAjaxServer.init();
            load = true;
        });

        function approvalSubmitAction($url) {
            $('#formApproval').attr('action', $url);
            $('#formApproval').submit();
        }


    </script>
@endpush
