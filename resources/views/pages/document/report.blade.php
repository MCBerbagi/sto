{{-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.min.css" integrity="sha512-zxBiDORGDEAYDdKLuYU9X/JaJo/DPzE42UubfBw9yg8Qvb2YRRIQ8v4KsGHOx2H1/+sdSXyXxLXv5r7tHc9ygg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<div class="modal fade" id="id_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form class="m-form m-form--fit m-form--label-align-right" id="form_data" action="{{route('document.report', ['id' => $document->id])}} " method="post">
            {{csrf_field()}}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Tambah Report ({{$document->nomor_tiket}})
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        &times;
                    </button>
                </div>
                <div class="modal-body">
                    {{-- Sebelum --}}
                    <div class="row">
                        <input type="hidden" name="report_id" value="{{ $document_detail->id }}">
                        <div class="col-md-6">
                            <label for="">Tanggal Awal Pengejaan</label>
                            <input type="text" class="form-control datetimepicker1" name="start_date" value="{{ $document_detail->start_date != null ?  DateTime::createFromFormat('Y-m-d H:i:s', $document_detail->start_date)->format('d/m/Y H:i') : ''}}">
                        </div>
                        <div class="col-md-6">
                            <label for="">Tanggal Akhir Pengejaan</label>
                            <input type="text" class="form-control datetimepicker2" name="end_date" value="{{ $document_detail->end_date != null ? DateTime::createFromFormat('Y-m-d H:i:s', $document_detail->end_date)->format('d/m/Y H:i') : '' }}">
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="" style="font-weight: bold; font-size: 15px; margin-top: 10px">
                                   Sebelum
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="file" name="sebelum1" class="dropify dropify-event" data-max-file-size="2000K" data-allowed-file-extensions="png jpg jpeg" value="{{ $document_detail->images_sebelum[0] ?? ''}}" data-default-file="{{ count($document_detail->images_sebelum) > 0 ? asset('storage/images/sebelum/'.$document_detail->images_sebelum[0]) : ''}}" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="file" name="sebelum2" class="dropify dropify-event" data-max-file-size="2000K" data-allowed-file-extensions="png jpg jpeg" value="{{ $document_detail->images_sebelum[1] ?? ''}}" data-default-file="{{ count($document_detail->images_sebelum) > 1 ?  asset('storage/images/sebelum/'.$document_detail->images_sebelum[1]) : ''}}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="file" name="sebelum3" class="dropify dropify-event" data-max-file-size="2000K" data-allowed-file-extensions="png jpg jpeg" value="{{ $document_detail->images_sebelum[2] ?? ''}}" data-default-file="{{ count($document_detail->images_sebelum) > 2 ?  asset('storage/images/sebelum/'.$document_detail->images_sebelum[2]) : ''}}" >
                            </div>
                        </div>
                        {{-- Proses --}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="" style="font-weight: bold; font-size: 15px">
                                   Proses
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="file" name="proses1" class="dropify dropify-event" data-max-file-size="2000K" data-allowed-file-extensions="png jpg jpeg" value="{{ $document_detail->images_proses[0] ?? ''}}" data-default-file="{{ count($document_detail->images_proses) > 0 ?  asset('storage/images/proses/'.$document_detail->images_proses[0]) : ''}}" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="file" name="proses2" class="dropify dropify-event" data-max-file-size="2000K" data-allowed-file-extensions="png jpg jpeg" value="{{ $document_detail->images_proses[1] ?? ''}}" data-default-file="{{ count($document_detail->images_proses) > 1 ?  asset('storage/images/proses/'.$document_detail->images_proses[1]) : ''}}" >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="file" name="proses3" class="dropify dropify-event" data-max-file-size="2000K" data-allowed-file-extensions="png jpg jpeg" value="{{ $document_detail->images_proses[2] ?? ''}}" data-default-file="{{ count($document_detail->images_proses) > 2 ?  asset('storage/images/proses/'.$document_detail->images_proses[2]) : ''}}" >
                            </div>
                        </div>
                        {{-- Sesudah --}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="" style="font-weight: bold; font-size: 15px">
                                   Sesudah
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="file" name="sesudah1" class="dropify dropify-event" data-max-file-size="2000K" data-allowed-file-extensions="png jpg jpeg" value="{{ $document_detail->images_sesudah[0] ?? ''}}" data-default-file="{{ count($document_detail->images_sesudah) > 0 ?  asset('storage/images/sesudah/'.$document_detail->images_sesudah[0]) : ''}}" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="file" name="sesudah2" class="dropify dropify-event" data-max-file-size="2000K" data-allowed-file-extensions="png jpg jpeg" value="{{ $document_detail->images_sesudah[1] ?? ''}}" data-default-file="{{ count($document_detail->images_sesudah) > 1 ? asset('storage/images/sesudah/'.$document_detail->images_sesudah[1]) : ''}}" >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="file" name="sesudah3" class="dropify dropify-event" data-max-file-size="2000K" data-allowed-file-extensions="png jpg jpeg" value="{{ $document_detail->images_sesudah[2] ?? ''}}" data-default-file="{{ count($document_detail->images_sesudah) > 2 ?  asset('storage/images/sesudah/'.$document_detail->images_sesudah[2]) : ''}}" >
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="status">
                                    Pilih Status
                                </label>
                                <select name="status" id="status" class="form-control select2me" required>
                                    <option value="">---pilih status---</option>
                                    @foreach ($status as $item)
                                        <option value="{{$item}}" {{ $document->status == $item ? 'selected' : ''}}>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="keterangan">
                                   Keterangan
                                </label>
                                <textarea name="keterangan" class="my-editor form-control" id="my-editor" cols="10" rows="10" style="height: 100px"></textarea>
                                {{-- <input type="text" name="keterangan" class="form-control" id="keterangan" value="{{ $document_detail->keterangan }}"> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-success" id="btn_submit">
                        Save changes
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<style>

</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.js" integrity="sha512-vUJTqeDCu0MKkOhuI83/MEX5HSNPW+Lw46BA775bAWIp1Zwgz3qggia/t2EnSGB9GoS2Ln6npDmbJTdNhHy1Yw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}
<script>
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.select2me').select2({
        width: '100%',
    });

    $('.summernote').summernote({
        height: 250
    });

    $(function() {
        $('.datetimepicker1').datetimepicker({
        format: 'dd/mm/yyyy hh:ii'
        });
    });

    $(function() {
        $('.datetimepicker2').datetimepicker({
        format: 'dd/mm/yyyy hh:ii'
        });
    });

    $('textarea').each(function () {
    this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    }).on('input', function () {
    this.style.height = 'auto';
    this.style.height = (this.scrollHeight) + 'px';
    });

    $('.dropify').dropify({
        height : "200",
        error: {
            'fileSize': 'The file size is too big (200 kb max).',
        },
        messages: {
            'default': 'Drag and drop a photo here or click',
            'replace': 'Drag and drop or click to replace',
            'remove':  'Remove',
            'error':   'Ooops, something wrong happended.'
        }
    });

    // var resize = $('.dropify').croppie({
    //     enableExif: true,
    //     enableOrientation: true,
    //     viewport: { // Default { width: 100, height: 100, type: 'square' }
    //         width: 200,
    //         height: 200,
    //         type: 'circle' //square
    //     },
    //     boundary: {
    //         width: 300,
    //         height: 300
    //     }
    // });


    // $('#images').on('change', function () {
    // var reader = new FileReader();
    //     reader.onload = function (e) {
    //     resize.croppie('bind',{
    //         url: e.target.result
    //     }).then(function(){
    //         console.log('jQuery bind complete');
    //     });
    //     }
    //     reader.readAsDataURL(this.files[0]);
    // });

    toastr.options = {
        "closeButton": true,
        "progressBar": true,
        "timeOut": "2000",
    };

    // Used events
    var drEvent = $('.dropify-event').dropify();
    drEvent.on('dropify.beforeClear', function(event, element) {
        return confirm("Do you really want to delete" + element.file.name + " ?");
    });
    drEvent.on('dropify.afterClear', function(event, element) {
        $.ajax({
            type: "POST",
            url: '{{route("document.remove-image")}}',
            data: {
                id: $("[name='report_id']").val(),
                path: $(`[name=${element.element.attributes.name.value}]`)[0].attributes.value.value
            }
        });
        toastr.success("File deleted", 'Success');
    });

    var options = {
        target: '.message',
        beforeSubmit: function () {
            $("#btn_submit").addClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $("#btn_submit").prop({disabled: true});
        },
        error: function (data) {
            clearValidation();

            $("#btn_submit").removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            $("#btn_submit").prop({disabled: false});
            const response = data.responseJSON;
            const errors = response.message;
            const items = Object.keys(errors);
            const firstName = items[0];
            const firstItemDOM = document.getElementById(firstName);

            firstItemDOM.scrollIntoView()

            for (let index = 0; index < items.length; index++) {
                const element = items[index];
                const message = errors[element];

                $('.' + element).addClass('has-error');
                $('[name="'+element+'"]').addClass('field-error');
                $('<span class="has-error">'+message+'</span>').insertAfter('[id="'+element+'"]');
            }

            swal.fire("Error", 'Silahkan periksa kembali data anda', "error")
        },
        success: function (data) {
            $("#btn_submit").removeClass("kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light");
            if (data.error) {
                $("#btn_submit").prop({disabled: false});
                swal.fire("Error", data.message, "error")
            } else {
                $(".modal").modal("hide");
                table.ajax.reload(null, false);
                swal.fire("Good job!", data.message, "success")
            }

        },
        dataType: 'json'
    };

    // pass options to ajaxForm
    $('#form_data').ajaxForm(options);
</script>
