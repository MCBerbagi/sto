@extends('layouts.master')
@section('title', 'List Document History')

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Lists</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
            <a href="" class="text-muted">Document History</a>
        </li>
        <li class="breadcrumb-item">
            <span class="text-muted">List</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::Mixed Widget 10-->
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">List Document History</h3>
                </div>
            </div>

            <div class="card-body">
                <table class="table table-separate table-head-custom table-checkable" id="datatable">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Dibuat</th>
                        <th>Nomor Tiket</th>
                        <th>STO</th>
                        <th>Mitra</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

            </div>
        </div>
        <!--end::Mixed Widget 10-->
    </div>
</div>

<form action="" method="POST" id="formApproval">
    @csrf
</form>
@endsection

@push('js-script')
    <script>
        $(document).ready(function () {
            $('#datatable').DataTable({
                scrollX: true,
                processing: true,
                serverSide: true,
                ajax: "{{ route('history.index') }}",
                columns: [
                    {data: null, sortable: false, searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {data: 'created_at', name: 'created_at'},
                    {data: 'nomor_tiket', name: 'nomor_tiket'},
                    {data: 'sto', name: 'sto'},
                    {data: 'mitra', name: 'mitra'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                columnDefs: [
                    { "className": "text-center", "targets": 2 },
                ]
            })
        });
    </script>
@endpush
