<!doctype html>
<html lang="en">
<head>
    <title>Document PDF</title>

    <style>
        body{
            font-size: 11px;
        }

        .container{
            /*margin: 10px;*/
            padding: 0 20px 20px 20px;
            /*border: 1px solid black;*/
        }

        table, tr {
            page-break-inside: auto;
            border-collapse: collapse;
            vertical-align: middle;
        }

        td {
            border-collapse: collapse;
            vertical-align: middle;
            padding: 5px 3px;
        }

        .mt-1{
            margin-top: 10px;
            margin-bottom: 20px;
        }

        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
        }
    </style>
</head>
<body>
<div class="container">
    <table>
        <tr>
            <td style="font-size: 18px">
                <b>Laporan Pekerjaan</b><br>
                <b>Gangguan Masal (GAMAS)</b><br>
            </td>
        </tr>
    </table>
    <hr>
    <table width="60%">
        <tr>
            <td>Tiket</td>
            <td>:</td>
            <td>{{ $document->nomor_tiket }}</td>
        </tr>
        <tr>
            <td>STO</td>
            <td>:</td>
            <td>{{ $document->sto }}</td>
        </tr>
        <tr>
            <td>Detail Gamas</td>
            <td>:</td>
            <td>{{ $document->detail_gamas }}</td>
        </tr>
        <tr>
            <td>Node Terganggu</td>
            <td>:</td>
            <td>{{ $document->node_terganggu }}</td>
        </tr>
        <tr>
            <td>Tanggal Laporan</td>
            <td>:</td>
            <td>{{ date('d/m/Y', strtotime($document->created_at)) }}</td>
        </tr>
        <tr>
            <td>Mitra</td>
            <td>:</td>
            <td>{{ $mitra->name }}</td>
        </tr>
        <tr>
            <td>Durasi Pengerjaan</td>
            <td>:</td>
            <td>{{ date('d/m/Y H:i', strtotime($document_detail->start_date)) }} - {{ date('d/m/Y H:i', strtotime($document_detail->end_date)) }}</td>
        </tr>
    </table>
    <div style="margin-top: 50px;margin-right: 20px;margin-bottom: 20px; text-align: center;">
        <h2>Evident</h2>
    </div>
    <hr style="clear: both;margin: 20px 0;border: 1px dashed black;">
    <div style="clear: both;display: block">
        <h2>Sebelum</h2>
        <div style="width: 100%;">
            @foreach ($path_img_sebelum as $image)
                <img src="{{ $image }}" class="mt-1" style="max-width: 80%; max-height: 300px">
            @endforeach
        </div>
        <h2 class="mt-4">Proses</h2>
        <div style="width: 100%;">
            @foreach ($path_img_proses as $image)
                <img src="{{ $image }}" class="mt-1" style="max-width: 80%; max-height: 300px">
            @endforeach
        </div>
        <h2 class="mt-4">Sesudah</h2>
        <div style="width: 100%;">
            @foreach ($path_img_sesudah as $image)
                <img src="{{ $image }}" class="mt-1" style="max-width: 80%; max-height: 300px">
            @endforeach
        </div>
    </div>
    <div style="clear: both;display: block">
        <h2 style="margin-top: 20px">Keterangan</h2>
        <span>{{ $document_detail->keterangan ?? "-" }}</span>
    </div>
</div>
</body>
</html>
