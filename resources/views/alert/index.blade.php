<script>
    toastr.options = {
        "closeButton": true,
        "progressBar": true,
        "timeOut": "6000",
    };

    @if(session('message'))
        toastr.success("{{ session('message') }}", 'Success');
    @endif

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            toastr.error("{{ $error }}", 'Error');
        @endforeach
    @endif
</script>
